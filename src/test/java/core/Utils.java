package core;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.github.javafaker.Faker;
import models.MapField;
import models.OrdersTableTaskAnnotated;
import org.openqa.selenium.By;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Utils {

    public static <K, V> Map<K, V> zipToMap(List<K> keys, List<V> values) {
        return IntStream.range(0, keys.size()).boxed()
                .collect(Collectors.toMap(keys::get, values::get));
    }

    public static <T> List<T> toList(ElementsCollection rowsCollection, List<String> headerTexts, By cellsChild, Class<T> tClass)
            throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        List<Object> resultList = new ArrayList<>();

        for (SelenideElement row : rowsCollection) {
            T task = tClass.getConstructor().newInstance();
            for (int i = 0; i < headerTexts.size(); i++) {
                SelenideElement cell = row.$$(cellsChild).get(i);
                String cellText = cell.text();
                String header = headerTexts.get(i);
                Field[] declaredFields = task.getClass().getDeclaredFields();

                for (Field field : declaredFields) {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(MapField.class)) {
                        MapField annotation = field.getAnnotation(MapField.class);
                        if (annotation.columnName().equals(header)) {
                            if (field.getType().equals(Integer.class)) {
                                Integer value = Integer.parseInt(cellText);
                                try {
                                    field.set(task, value);
                                } catch (IllegalAccessException e) {
                                    throw new RuntimeException(e);
                                }
                            } else if (field.getType().equals(Double.class)) {
                                Double value = Double.valueOf(cellText.replaceAll("\\$", ""));
                                try {
                                    field.set(task, value);
                                } catch (IllegalAccessException e) {
                                    throw new RuntimeException(e);
                                }
                            } else if (field.getType().equals(Date.class) & !cellText.isEmpty()) {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                try {
                                    Date value = dateFormat.parse(cellText);
                                    field.set(task, value);
                                } catch (IllegalAccessException | ParseException e) {
                                    throw new RuntimeException(e);
                                }
                            } else if (field.getType().equals(Boolean.class)) {
                                Boolean value = OrdersTableTaskAnnotated.parseCellTextToBoolean(cellText);
                                try {
                                    field.set(task, value);
                                } catch (IllegalAccessException e) {
                                    throw new RuntimeException(e);
                                }
                            } else if (field.getType().equals(String.class)) {
                                try {
                                    field.set(task, cellText);
                                } catch (IllegalAccessException e) {
                                    throw new RuntimeException(e);
                                }
                            } else {
                                System.out.println("Cell is empty (nothing to write). " + "Header: " + header + ". The row is: " + row.getText() );
                            }
                        }
                    }
                }
            }
            resultList.add(task);
        }
        return (List<T>) resultList;
    }

    private final static Faker faker = new Faker();

    public static String randomTxtString() {
        return faker.rickAndMorty().character() + faker.random().nextInt(100500) + ".txt";
    }

    public static String randomSentences(int sentences) {
        return faker.lorem().paragraph(sentences);
    }
}
