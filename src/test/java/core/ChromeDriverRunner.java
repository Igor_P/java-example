package core;

import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverRunner {

    public static void setChromeDriver() {
        ChromeDriver chromeDriver = new ChromeDriver();
        WebDriverRunner.setWebDriver(chromeDriver);
    }
}
