package core;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class Base {

    @BeforeSuite()
    public void beforeSuite() {
        Configuration.timeout = 10000;
        Configuration.baseUrl = "https://the-internet.herokuapp.com";
    }

    @AfterMethod()
    public void afterMethod() {
        Selenide.closeWebDriver();
    }
}
