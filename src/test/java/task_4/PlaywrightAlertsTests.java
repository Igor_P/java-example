package task_4;

import com.microsoft.playwright.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class PlaywrightAlertsTests {

    private Page page;
    private JsAlertsPage jsAlertsPage;

    @BeforeMethod
    public void beforeMethod() {
        Playwright playwright = Playwright.create();
        page = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false)).newPage();
        page.navigate("https://the-internet.herokuapp.com/javascript_alerts");
        jsAlertsPage = new JsAlertsPage(page);
    }

    @Test
    public void jsPromptAlert() {
        String promptText = "Test value";
        page.onceDialog(alert -> {
            alert.accept(promptText);
        });
        assertThat(jsAlertsPage.getJsPromptButton()).hasText("Click for JS Prompt");
        jsAlertsPage.clickJsPromptButton();
        assertThat(jsAlertsPage.getResult()).hasText("You entered: " + promptText);
    }
}
