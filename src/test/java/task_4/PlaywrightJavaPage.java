package task_4;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class PlaywrightJavaPage {

    private final Page page;

    public PlaywrightJavaPage(Page page) {
        this.page = page;
    }

    public Locator getResilientBlock() {
        return page.locator("(//div[@class='col col--6'])[4]");
    }
}
