package task_4.setup;

import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class PlaywrightSetup {

    Playwright playwright;
    Page page;

    public Page getPage() {
        return page;
    }

    @BeforeMethod
    public void setup() {
        playwright = Playwright.create();
        page = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false)).newPage();
    }

    @AfterMethod
    public void tearDown() {
        playwright.close();
    }
}
