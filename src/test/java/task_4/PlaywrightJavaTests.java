package task_4;

import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.options.BoundingBox;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class PlaywrightJavaTests {
    private Page page;
    private PlaywrightJavaPage playwrightJavaPage;

    @BeforeMethod
    public void beforeMethod() {
        Playwright playwright = Playwright.create();
        page = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false)).newPage();
        page.navigate("https://playwright.dev/java/");
        playwrightJavaPage = new PlaywrightJavaPage(page);
    }

    @Test
    public void scrollToResilientText() {
        BoundingBox box = playwrightJavaPage.getResilientBlock().boundingBox();
        assertThat(playwrightJavaPage.getResilientBlock()).not().isInViewport();
        page.mouse().wheel(0, box.y);
        assertThat(playwrightJavaPage.getResilientBlock()).isInViewport();
    }
}
