package task_4;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class JsAlertsPage {

    private final Page page;

    public JsAlertsPage(Page page) {
        this.page = page;
    }

    public void clickJsPromptButton() {
        getJsPromptButton().click();
    }

    public Locator getJsPromptButton() {
        return page.locator("//*[@onclick ='jsPrompt()']");
    }

    public Locator getResult() {
        return page.locator("//*[@id='result']");
    }
}
