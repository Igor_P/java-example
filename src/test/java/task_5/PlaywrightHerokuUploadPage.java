package task_5;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

import java.io.File;

public class PlaywrightHerokuUploadPage {

    private final Page page;

    public PlaywrightHerokuUploadPage(Page page) {
        this.page = page;
    }

    public Locator chooseFileButton() {
        return page.locator("#file-upload");
    }

    public Locator uploadFileButton() {
        return page.locator("#file-submit");
    }

    public Locator uploadedFile() {
        return page.locator("#uploaded-files");
    }

    public void uploadFile(File fileName) {
        chooseFileButton().setInputFiles(fileName.toPath());
    }

    public void openPage() {
        page.navigate("https://the-internet.herokuapp.com/upload");
    }

}
