package task_5;

import com.microsoft.playwright.Download;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;

import java.io.File;

public class PlaywrightHerokuDownloadPage {

    private final Page page;

    public PlaywrightHerokuDownloadPage(Page page) {
        this.page = page;
    }

    public void openPage() {
        page.navigate("https://the-internet.herokuapp.com/download");
    }

    public Download clickLinkByName(File fileName) {
        return page.waitForDownload(() -> {
            page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName(fileName.getName())).click();
        });
    }
}
