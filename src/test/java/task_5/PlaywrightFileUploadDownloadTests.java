package task_5;

import core.Utils;
import com.microsoft.playwright.*;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import task_4.setup.PlaywrightSetup;

import java.io.File;
import java.io.IOException;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class PlaywrightFileUploadDownloadTests extends PlaywrightSetup {
    private PlaywrightHerokuDownloadPage playwrightHerokuDownloadPage;
    private PlaywrightHerokuUploadPage playwrightHerokuUploadPage;
    private final String pathToSaveFile = "target/random-files/";
    private final String pathToDownloadFile = "target/downloaded-files/";


    @BeforeMethod
    public void beforeMethod() {
        playwrightHerokuUploadPage = new PlaywrightHerokuUploadPage(getPage());
        playwrightHerokuDownloadPage = new PlaywrightHerokuDownloadPage(getPage());
        playwrightHerokuUploadPage.openPage();
    }

    @Test
    public void uploadTest() throws IOException {
        String randomText = Utils.randomSentences(2);
        File fileUpload = new File(pathToSaveFile + Utils.randomTxtString());
        FileUtils.writeStringToFile(fileUpload, randomText);
        playwrightHerokuUploadPage.uploadFile(fileUpload);
        playwrightHerokuUploadPage.uploadFileButton().click();
        assertThat(playwrightHerokuUploadPage.uploadedFile()).hasText(fileUpload.getName());
    }

    @Test
    public void downloadTest() throws IOException {
        String randomText = Utils.randomSentences(3);
        File fileUpload = new File(pathToSaveFile + Utils.randomTxtString());
        FileUtils.writeStringToFile(fileUpload, randomText);
        playwrightHerokuUploadPage.uploadFile(fileUpload);
        playwrightHerokuUploadPage.uploadFileButton().click();
        playwrightHerokuDownloadPage.openPage();
        Download download = playwrightHerokuDownloadPage.clickLinkByName(fileUpload);
        File fileDownload = new File(pathToDownloadFile, download.suggestedFilename());
        download.saveAs(fileDownload.toPath());
        Assert.assertTrue(FileUtils.contentEquals(fileUpload, fileDownload));
    }
}
