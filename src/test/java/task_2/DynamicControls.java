package task_2;

import com.codeborne.selenide.Configuration;
import core.Base;
import core.ChromeDriverProvider;
import core.ChromeDriverRunner;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static pages.DynamicControlsPage.*;
import static pages.JsAlertsPage.*;

public class DynamicControls extends Base {

    @Test
    public void test1() {
        String textForInput = "Test text";
        Configuration.browser = "firefox";
        openDynamicControlsPage();
        waitInputFieldStatus(false);
        clickEnableButton();
        waitInputFieldStatus(true);
        fillInputField(textForInput);
        assertEquals(inputField().getValue(), textForInput);
        clickDisableButton();
        waitInputFieldStatus(false);
        assertEquals(message().getText(), "It's disabled!");
    }

    @Test
    public void test2() {
        ChromeDriverRunner.setChromeDriver();
        openDynamicControlsPage();
        checkboxIsVisibleAndEnabled();
        checkboxCheckedOrNot(false);
        clickCheckbox();
        checkboxCheckedOrNot(true);
        clickRemoveButton();
        waitForLoaderDisappear(15);
        assertTrue(checkbox().is(not(visible)), "Checkbox should not be visible");
        clickAddButton();
        waitForLoaderDisappear(15);
        assertTrue(checkbox().is(visible), "Checkbox should be visible");
        assertEquals(message().getText(), "It's back!");
    }

    @Test
    public void test3() {
        Configuration.browser = ChromeDriverProvider.class.getName();
        openJavascriptAlertsPage();
        clickJsAlertByFilter();
        assertEquals(handleAlertAndGetText(true), "I am a JS Alert");
        assertEquals(getResultText(), "You successfully clicked an alert");
    }
}