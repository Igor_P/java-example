package task_6;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import task_6.api_models.payloads.EmployeeUpdatePayload;
import task_6.api_models.responses.employee_update.EmployeeUpdateResponse;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

public class EmployeeTests {

    @BeforeMethod
    public void beforeMethod() {
        RestAssured.baseURI = "https://dummy.restapiexample.com/api/v1";
    }

    @Test
    public void updateEmployeeById() {
        String employeeId = "1000";
        EmployeeUpdatePayload body = new EmployeeUpdatePayload()
                .builder()
                .name("John")
                .salary("200000")
                .age("30").build();
        EmployeeUpdateResponse response = given()
                .contentType(ContentType.JSON)
                .body(body)
                .put("/update/{id}", employeeId)
                .then()
                .statusCode(200)
                .extract()
                .as(EmployeeUpdateResponse.class);
        assertEquals(response.getMessage(), "Successfully! Record has been updated.");
    }

    @Test
    public void updateEmployeeByIdWithBodyLogsIfFail() {
        String employeeId = "1000";
        EmployeeUpdatePayload body = new EmployeeUpdatePayload()
                .builder()
                .name("John")
                .salary("200000")
                .age("30").build();
        Response response = given()
                .contentType(ContentType.JSON)
                .body(body)
                .put("/update/{id}", employeeId)
                .then()
                .extract()
                .response();
        assertEquals(response.getStatusCode(), 200, "Status code is: " + response.getStatusCode() + ". Body: "
                + response.getBody().prettyPrint());
        EmployeeUpdateResponse employeeUpdateResponse = response.as(EmployeeUpdateResponse.class);
        assertEquals(employeeUpdateResponse.getMessage(), "Successfully! Record has been updated.");
    }
}
