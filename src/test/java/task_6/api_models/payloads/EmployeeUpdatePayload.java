package task_6.api_models.payloads;

import lombok.*;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EmployeeUpdatePayload {

    private String name;
    private String salary;
    private String age;
}
