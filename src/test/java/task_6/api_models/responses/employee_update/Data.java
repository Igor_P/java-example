package task_6.api_models.responses.employee_update;

import lombok.*;


@Getter
public class Data {

    private String name;
    private String salary;
    private String age;
}
