package task_6.api_models.responses.employee_update;

import lombok.*;


@Getter
public class EmployeeUpdateResponse {

    private String status;
    private Data data;
    private String message;
}
