package pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import core.Utils;
import org.openqa.selenium.By;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$$x;

public class OrdersTablePage {

    public static ElementsCollection headers = $$x("//table//th");

    public static ElementsCollection rows = $$x("//table//tbody//tr");

    public static By cellsChild = By.xpath(".//td");

    public static List<Map<String, String>> tableToList() {
        headers.shouldBe(CollectionCondition.sizeGreaterThan(0));
        List<Map<String, String>> resultTable = new LinkedList<>();
        List<String> headerTexts = headers.texts();

        for (SelenideElement row : OrdersTablePage.rows) {
            List<String> cellsTexts = row.$$(cellsChild).texts();
            System.out.println(cellsTexts);
            resultTable.add(Utils.zipToMap(headerTexts, cellsTexts));
        }
        return resultTable;
    }
}
