package pages;

import com.codeborne.selenide.*;
import org.openqa.selenium.Alert;

import static com.codeborne.selenide.Selenide.*;

public class JsAlertsPage {

    public static void openJavascriptAlertsPage() {
        Selenide.open("/javascript_alerts");
    }

    public static ElementsCollection buttons = $$x("//button");

    public static void clickJsAlertButton() {
        $x("//*[@onclick ='jsAlert()']").shouldBe(Condition.visible).click();
    }

    public static void clickJsConfirmButton() {
        $x("//*[@onclick ='jsConfirm()']").shouldBe(Condition.visible).click();
    }

    public static void clickJsPromptButton() {
        $x("//*[@onclick ='jsPrompt()']").shouldBe(Condition.visible).click();
    }

    public static String handleAlertAndGetText(Boolean accept, String... sendText) {
        Alert alert = switchTo().alert();
        String alertText = alert.getText();
        if (sendText.length > 0) {
            alert.sendKeys(sendText[0]);
        }
        if (accept) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return alertText;
    }

    public static String getResultText() {
        return $x("//*[@id='result']").shouldBe(Condition.visible).getText();
    }

    public static String getResultTextByJsExec() {
        return executeJavaScript("return document.getElementById('result').textContent");
    }

    public static void clickJsConfirmButtonByJs() {
        $x("//*[@onclick ='jsConfirm()']").shouldBe(Condition.visible).click(ClickOptions.usingJavaScript());
    }

    public static void clickJsConfirmButtonByJsExec() {
        executeJavaScript("document.querySelector(\"button[onclick='jsConfirm()']\").click()");
    }

    public static void clickJsAlertByFilter() {
        buttons.filter(Condition.attribute("onclick", "jsAlert()"))
                .first()
                .shouldBe(Condition.visible)
                .click();
    }
}
