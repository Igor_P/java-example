package pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;

public class DynamicControlsPage {

    public static void openDynamicControlsPage() {
        Selenide.open("/dynamic_controls");
    }

    public static SelenideElement inputField() {
        return $x("//input[@type='text']").shouldBe(visible);
    }

    public static SelenideElement enableButton() {
        return $x("//button[@onclick='swapInput()' and text()='Enable']").shouldBe(visible);
    }

    public static SelenideElement disableButton() {
        return $x("//button[@onclick='swapInput()' and text()='Disable']").shouldBe(visible);
    }

    public static SelenideElement removeButton() {
        return $x("//button[@onclick='swapCheckbox()' and text()='Remove']").shouldBe(visible);
    }

    public static SelenideElement addButton() {
        return $x("//button[@onclick='swapCheckbox()' and text()='Add']").shouldBe(visible);
    }

    public static SelenideElement message() {
        return $x("//*[@id='message']").shouldBe(visible);
    }

    public static SelenideElement checkbox() {
        return $x("//input[@type='checkbox']");
    }

    public static SelenideElement loader() {
        return $x("//*[@id='loading']").shouldBe(visible);
    }

    public static void waitForLoaderDisappear(int timeout) {
        loader().shouldBe(not(visible), Duration.ofSeconds(timeout));
    }


    public static void clickRemoveButton() {
        removeButton().click();
    }

    public static void clickAddButton() {
        addButton().click();
    }

    public static void checkboxIsVisibleAndEnabled() {
        checkbox().shouldBe(visible).shouldBe(enabled);
    }

    public static void checkboxCheckedOrNot(boolean shouldBeChecked) {
        if (shouldBeChecked) {
            checkbox().shouldBe(checked);
        } else {
            checkbox().shouldNotBe(checked);
        }
    }

    public static void clickCheckbox() {
        checkbox().shouldBe(visible).click();
    }

    public static void waitInputFieldStatus(boolean shouldBeEnabled) {
        if (shouldBeEnabled) {
            inputField().shouldBe(enabled);
        } else {
            inputField().shouldBe(disabled);
        }
    }

    public static void clickEnableButton() {
        enableButton().click();
    }

    public static void clickDisableButton() {
        disableButton().click();
    }

    public static void fillInputField(String textForInput) {
        inputField().sendKeys(textForInput);
    }
}
