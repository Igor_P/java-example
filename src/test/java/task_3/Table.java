package task_3;

import com.codeborne.selenide.CollectionCondition;
import core.Utils;
import models.OrdersTableTaskAnnotated;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Selenide.open;
import static models.OrdersTableTaskAnnotated.formatDateToString;
import static org.testng.Assert.assertEquals;
import static pages.OrdersTablePage.*;

public class Table {

    @Test
    public void byZipToMap() {
        open("file://"
                + Table.class.getClassLoader().getResource("orders_table.html").getPath());

        List<Map<String, String>> resultTable = tableToList();

        assertEquals(resultTable.get(0).get("Order ID"), "1");
        assertEquals(resultTable.get(0).get("User email"), "user1@example.com");
        assertEquals(resultTable.get(0).get("User name"), "John Doe");
        assertEquals(resultTable.get(0).get("Created date"), "2024-03-01");
        assertEquals(resultTable.get(0).get("Items count"), "3");
        assertEquals(resultTable.get(0).get("Is delivered"), "Yes");
        assertEquals(resultTable.get(0).get("Is payed"), "Yes");
        assertEquals(resultTable.get(0).get("Delivered date"), "2024-03-05");
        assertEquals(resultTable.get(0).get("Amount"), "$50.00");
        assertEquals(resultTable.get(0).get("Status"), "Completed");
    }

    @Test
    public void byAnnotations() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        open("file://"
                + this.getClass().getClassLoader().getResource("orders_table.html").getPath());

        headers.shouldBe(CollectionCondition.sizeGreaterThan(0));

        List<OrdersTableTaskAnnotated> resultList = Utils.toList(rows, headers.texts(), cellsChild, OrdersTableTaskAnnotated.class);
        OrdersTableTaskAnnotated ninthOrder = resultList.stream()
                .filter(p -> p.getOrderId().equals(9))
                .findFirst()
                .get();

        assertEquals(ninthOrder.getUserEmail(), "user9@example.com");
        assertEquals(ninthOrder.getUserName(), "Matthew Martinez");
        assertEquals(formatDateToString(ninthOrder.getCreatedDate()), "2024-03-09");
        assertEquals(ninthOrder.getItemsCount(), 5);
        assertEquals(ninthOrder.getDelivered(), true);
        assertEquals(ninthOrder.getPayed(), false);
        assertEquals(formatDateToString(ninthOrder.getDeliveredDate()), "2024-03-12");
        assertEquals(ninthOrder.getAmount(), 90.00);
        assertEquals(ninthOrder.getStatus(), "Processing");
    }
}
