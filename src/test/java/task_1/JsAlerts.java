package task_1;

import core.Base;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static pages.JsAlertsPage.*;

public class JsAlerts extends Base {

    @BeforeMethod
    public void beforeMethod() {
        openJavascriptAlertsPage();
    }

    @Test
    public void jsAlert() {
        clickJsAlertButton();
        Assert.assertEquals(handleAlertAndGetText(true), "I am a JS Alert");
        Assert.assertEquals(getResultText(), "You successfully clicked an alert");
    }

    @Test
    public void jsConfirm() {
        clickJsConfirmButton();
        Assert.assertEquals(handleAlertAndGetText(false), "I am a JS Confirm");
        Assert.assertEquals(getResultText(), "You clicked: Cancel");
    }

    @Test
    public void jsPrompt() {
        clickJsPromptButton();
        Assert.assertEquals(handleAlertAndGetText(true, "Test message"), "I am a JS prompt");
        Assert.assertEquals(getResultText(), "You entered: Test message");
    }

    @Test
    public void jsConfirmByJs() {
        clickJsConfirmButtonByJs();
        Assert.assertEquals(handleAlertAndGetText(true), "I am a JS Confirm");
        Assert.assertEquals(getResultText(), "You clicked: Ok");
    }

    @Test
    public void jsPromptByJsExecutor() {
        clickJsConfirmButtonByJsExec();
        Assert.assertEquals(handleAlertAndGetText(true), "I am a JS Confirm");
        Assert.assertEquals(getResultTextByJsExec(), "You clicked: Ok");
    }
}
