package models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OrdersTableTaskAnnotated {

    @MapField(columnName = "Order ID")
    private Integer orderId;
    @MapField(columnName = "User email")
    private String userEmail;
    @MapField(columnName = "User name")
    private String userName;
    @MapField(columnName = "Created date")
    private Date createdDate;
    @MapField(columnName = "Items count")
    private Integer itemsCount;
    @MapField(columnName = "Is delivered")
    private Boolean isDelivered;
    @MapField(columnName = "Is payed")
    private Boolean isPayed;
    @MapField(columnName = "Delivered date")
    private Date deliveredDate;
    @MapField(columnName = "Amount")
    private Double amount;
    @MapField(columnName = "Status")
    private String status;

    public Integer getOrderId() {
        return orderId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Integer getItemsCount() {
        return itemsCount;
    }

    public Boolean getDelivered() {
        return isDelivered;
    }

    public Boolean getPayed() {
        return isPayed;
    }

    public Date getDeliveredDate() {
        return deliveredDate;
    }

    public static String formatDateToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    public Double getAmount() {
        return amount;
    }

    public String getStatus() {
        return status;
    }

    public static Boolean parseCellTextToBoolean(String cellText) {
        return cellText.equals("Yes");
    }

    @Override
    public String toString() {
        return "TaskAnnotated{" +
                "orderId=" + orderId +
                ", userEmail='" + userEmail + '\'' +
                ", userName='" + userName + '\'' +
                ", createdDate=" + createdDate +
                ", itemsCount=" + itemsCount +
                ", isDelivered=" + isDelivered +
                ", isPayed=" + isPayed +
                ", deliveredDate=" + deliveredDate +
                ", amount=" + amount +
                ", status='" + status + '\'' +
                '}';
    }
}
